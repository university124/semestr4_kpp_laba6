module semy.task_2_client {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;

    opens semy.task_2_client to javafx.fxml;
    exports semy.task_2_client;
}