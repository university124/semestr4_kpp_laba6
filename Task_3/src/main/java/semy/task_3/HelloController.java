package semy.task_3;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import semy.task_3.bulletinBoardService.Messanger;
import semy.task_3.bulletinBoardService.MessangerImpl;
import semy.task_3.bulletinBoardService.UITasks;
import semy.task_3.bulletinBoardService.UITasksImpl;
import semy.task_3.bulletinBoardService.JavaFXInvocationHandler;

import java.lang.reflect.Proxy;
import java.net.InetAddress;

public class HelloController {

    @FXML
    private TextField messageTextField;

    @FXML
    private TextField groupTextField;

    @FXML
    private TextField portTextField;

    @FXML
    private TextField nameTextField;

    @FXML
    private TextArea chatTextArea;

    @FXML
    private Button connectButton;

    @FXML
    private Button disconnectButton;

    private Messanger messanger;

    @FXML
    public void initialize() {
        // Default values for group and port
        groupTextField.setText("224.0.0.1");
        portTextField.setText("3456");

        // Set initial button states
        connectButton.setDisable(false);
        disconnectButton.setDisable(true);
    }

    @FXML
    public void onConnectButtonClicked() {
        try {
            String name = nameTextField.getText();
            InetAddress addr = InetAddress.getByName(groupTextField.getText());
            int port = Integer.parseInt(portTextField.getText());

            UITasks ui = (UITasks) Proxy.newProxyInstance(getClass().getClassLoader(),
                    new Class[]{UITasks.class},
                    new JavaFXInvocationHandler(new UITasksImpl(chatTextArea, messageTextField)));

            messanger = new MessangerImpl(addr, port, name, ui);
            messanger.start();

            // Update button states
            connectButton.setDisable(true);
            disconnectButton.setDisable(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void onDisconnectButtonClicked() {
        if (messanger != null) {
            messanger.stop();
            messanger = null;

            // Update button states
            connectButton.setDisable(false);
            disconnectButton.setDisable(true);
        }
    }

    @FXML
    public void onSendButtonClicked() {
        if (messanger != null) {
            messanger.send();
        }
    }

    @FXML
    public void onClearButtonClicked() {
        chatTextArea.clear();
    }

    @FXML
    public void onExitButtonClicked() {
        System.exit(0);
    }
}
