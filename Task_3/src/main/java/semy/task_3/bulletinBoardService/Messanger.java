package semy.task_3.bulletinBoardService;

public interface Messanger {
    void start();
    void stop();
    void send();
}
