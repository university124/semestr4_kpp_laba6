package semy.task_3.bulletinBoardService;

public interface UITasks {
    String getMessage();
    void setText(String txt);
}
