package semy.task_3.bulletinBoardService;

import javafx.application.Platform;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class JavaFXInvocationHandler implements InvocationHandler {
    private Object invocationResult = null;
    private UITasks ui;

    public JavaFXInvocationHandler(UITasks ui) {
        this.ui = ui;
    }

    @Override
    public Object invoke(Object proxy, final Method method, final Object[] args) throws Throwable {
        if (Platform.isFxApplicationThread()) {
            invocationResult = method.invoke(ui, args);
        } else {
            Platform.runLater(() -> {
                try {
                    invocationResult = method.invoke(ui, args);
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
            });
        }
        return invocationResult;
    }
}
