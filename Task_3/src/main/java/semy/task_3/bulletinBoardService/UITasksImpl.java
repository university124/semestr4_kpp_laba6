package semy.task_3.bulletinBoardService;

import javafx.application.Platform;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class UITasksImpl implements UITasks {
    private TextArea textArea;
    private TextField textFieldMsg;

    public UITasksImpl(TextArea textArea, TextField textFieldMsg) {
        this.textArea = textArea;
        this.textFieldMsg = textFieldMsg;
    }

    @Override
    public String getMessage() {
        String res = textFieldMsg.getText();
        textFieldMsg.clear();
        return res;
    }

    @Override
    public void setText(final String txt) {
        Platform.runLater(() -> textArea.appendText(txt + "\n"));
    }
}
