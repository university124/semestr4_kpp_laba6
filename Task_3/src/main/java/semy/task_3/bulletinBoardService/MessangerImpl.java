package semy.task_3.bulletinBoardService;

import javafx.application.Platform;
import javafx.scene.control.Alert;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.HashSet;
import java.util.Set;

public class MessangerImpl implements Messanger {
    private UITasks ui;
    private MulticastSocket group;
    private InetAddress addr;
    private int port;
    private String name;
    private boolean canceled = false;
    private final Set<String> receivedMessages = new HashSet<>();

    public MessangerImpl(InetAddress addr, int port, String name, UITasks ui) {
        this.name = name;
        this.ui = ui;
        this.addr = addr;
        this.port = port;
        try {
            group = new MulticastSocket(port);
            group.setTimeToLive(2);
            group.joinGroup(addr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start() {
        Thread t = new Receiver();
        t.start();
    }

    @Override
    public void stop() {
        cancel();
        try {
            group.leaveGroup(addr);
        } catch (IOException e) {
            showAlert("Error", "Помилка від'єднання\n" + e.getMessage());
        } finally {
            group.close();
        }
    }

    @Override
    public void send() {
        new Sender().start();
    }

    private class Sender extends Thread {
        public void run() {
            try {
                String msg = name + ": " + ui.getMessage();
                if (msg != null && !msg.trim().isEmpty()) {
                    byte[] out = msg.getBytes();
                    DatagramPacket pkt = new DatagramPacket(out, out.length, addr, port);
                    group.send(pkt);
                }
            } catch (Exception e) {
                showAlert("Error", "Помилка відправлення\n" + e.getMessage());
            }
        }
    }

    private class Receiver extends Thread {
        public void run() {
            try {
                byte[] in = new byte[512];
                DatagramPacket pkt = new DatagramPacket(in, in.length);
                while (!isCanceled()) {
                    group.receive(pkt);
                    String received = new String(pkt.getData(), 0, pkt.getLength());
                    synchronized (receivedMessages) {
                        if (!receivedMessages.contains(received)) {
                            receivedMessages.add(received);
                            ui.setText(received);
                        }
                    }
                }
            } catch (Exception e) {
                if (isCanceled()) {
                    showAlert("Info", "З'єднання завершено");
                } else {
                    showAlert("Error", "Помилка прийому\n" + e.getMessage());
                }
            }
        }
    }

    private synchronized boolean isCanceled() {
        return canceled;
    }

    public synchronized void cancel() {
        canceled = true;
    }

    private void showAlert(String title, String message) {
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle(title);
            alert.setContentText(message);
            alert.showAndWait();
        });
    }
}
