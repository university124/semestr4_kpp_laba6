module semy.task_2_server {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;

    opens semy.task_2_server to javafx.fxml;
    exports semy.task_2_server;
}